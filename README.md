# How to run

Just open the project, do:

`pip install requirements.txt`

and run the project with:

`flask --app app/main.py --debug run`

It should work and it will also create the DB for you with already some entries when you load the index/root page.

# Description

+ A very simple login page that validates user and password. (root, root)
+ A home page with a video.
+ A query of pokemon. This uses the pokeapi.
+ A query of your captured pokemon. This uses the local DB.
+ You can add a new captured pokemon.
+ You can delete captured pokemons.
+ You can edit captured pokemons.

# Images

## Login

![login](/media/login.png)

## Home

![home](/media/pokemonHome.png)

## Pokemon Query

![pokemonQuery](/media/pokemonQuery.png)

## My Team

![myTeamQuery](/media/myTeamQuery.png)

## Add Captured Pokemon

![myTeamAdd](/media/myTeamAdd.png)

## Edit Captured Pokemon

![myTeamEdit](/media/myTeamEdit.png)
