
from sqlalchemy.exc import OperationalError
from werkzeug.exceptions import BadRequestKeyError
from app import create_app
from flask import render_template, request, flash, redirect, url_for
from app.models import *
from app.migrate import init_db
import requests

app = create_app()

@app.route("/")
def db_base():
    init_db()
    return redirect(url_for("login"))


@app.route("/home", methods = ("GET", "POST"))
def home():
    return render_template("home.html")

@app.route("/login", methods = ("GET", "POST"))
def login():
    if request.method == "POST":
        user_ = request.form["user"]
        password_ = request.form["password"]

        try:
            users = User.query.all()
            users = User.query.filter(User.user == user_, User.password == password_)
        except OperationalError:
            init_db()
            users = User.query.filter(User.user == user_, User.password == password_)
        
        try:
            matchingUser = users[0]
            return redirect(url_for("home"))
        except IndexError:
            flash("Password or username incorrect!")
        
    return render_template("login.html")

@app.route("/pokemons/", methods = ("GET", "POST"))
@app.route("/pokemons/<ammount>&&<selectable>", methods = ("GET", "POST"))
def pokemons(ammount=10, selectable=False):
    global loading

    if selectable != False and selectable == "True":
        return redirect(url_for("myTeamAdd", ammount=ammount, selectable=True))

    intAmmount = int(ammount)
    if intAmmount > pokemonNumberLimit:
        intAmmount = pokemonNumberLimit
    
    if not loading:
        loading = True
        for id in range(1, intAmmount + 1):
            if alreadyInPokemons(id):
                continue
            pokeapi_url = f"https://pokeapi.co/api/v2/pokemon/{id}/"
            r = requests.get(pokeapi_url)
            pokemonJsonData = r.json()
            pokemons.append(pokemonJsonData)
        loading = False
    else:
        intAmmount -= 21

    pokemonsToRender = intAmmount
    if len(pokemons) > intAmmount:
        pokemonsToRender = len(pokemons)

    return render_template("pokemons.html", pokemons=pokemons[0:pokemonsToRender], scale=0.7, ammount=pokemonsToRender, selectable=False)

@app.route("/myTeam", methods = ("GET", "POST"))
@app.route("/myTeam/<ammount>&&<selectable>", methods = ("GET", "POST"))
def myTeam(ammount=10, selectable=True):
    if request.method == "POST":
        if request.form.get("addPokemon") == "add":
            return redirect(url_for("myTeamAdd"))

    intAmmount = int(ammount)
    if intAmmount > pokemonNumberLimit:
        intAmmount = pokemonNumberLimit
    
    for id in range(1, intAmmount + 1):
        if alreadyInPokemons(id):
            continue
        pokeapi_url = f"https://pokeapi.co/api/v2/pokemon/{id}/"
        r = requests.get(pokeapi_url)
        pokemonJsonData = r.json()
        pokemons.append(pokemonJsonData)

    capturedPokemons = CapturedPokemon.query.all()

    return render_template("myTeam.html", pokemons=pokemons, capturedPokemons=capturedPokemons, scale=1, ammount=intAmmount)

@app.route("/myTeam/add", methods = ("GET", "POST"))
@app.route("/myTeam/add/<ammount>", methods = ("GET", "POST"))
def myTeamAdd(ammount=10):
    global loading

    intAmmount = int(ammount)
    if intAmmount > pokemonNumberLimit:
        intAmmount = pokemonNumberLimit
    
    if not loading:
        loading = True
        for id in range(1, intAmmount + 1):
            if alreadyInPokemons(id):
                continue
            pokeapi_url = f"https://pokeapi.co/api/v2/pokemon/{id}/"
            r = requests.get(pokeapi_url)
            pokemonJsonData = r.json()
            pokemons.append(pokemonJsonData)
        loading = False
    else:
        intAmmount -= 21

    pokemonsToRender = intAmmount
    if len(pokemons) > intAmmount:
        pokemonsToRender = len(pokemons)

    return render_template("pokemons.html", pokemons=pokemons[0:pokemonsToRender], scale=0.7, ammount=pokemonsToRender, selectable=True)
    
@app.route("/myTeam/add/data/<id>", methods = ("GET", "POST"))
def myTeamAddData(id=1):
    intId = int(id)
    if request.method == "POST":
        nickname = request.form["nickname"]
        level = request.form["level"]
        try:
            isShiny = request.form["isShiny"]
            isShiny = True
            print("Is Shiny: " + str(isShiny))
        except BadRequestKeyError:
            isShiny = False
            print("BadRequestKeyError")
            pass

        if not nickname:
            nickname = pokemons[intId - 1]["name"].capitalize()

        isShinyNumber = 0
        if isShiny:
            isShinyNumber = 1

        if not level or int(level) < 1 or int(level) > 100:
            flash("Please provide a level in the range 1-100")
        else:
            capturedPokemon = CapturedPokemon(
                idPokemon = id,
                nickname = nickname,
                level = level,
                isShiny = isShinyNumber
            )
            db.session.add(capturedPokemon)
            db.session.commit()
            flash("Pokemon added successfully!")
            return redirect(url_for("myTeam"))

    intId = int(id)
    pokemon = pokemons[intId - 1]
    return render_template("pokemonAdd.html", pokemon=pokemon, scale=1.6, capturedPokemonToEdit=None)

@app.route("/myTeam/delete/<id>", methods = ("GET", "POST"))
def myTeamDelete(id):
    intId = int(id)
    print("ID: " + str(intId))
    capturedPokemonToDelete = CapturedPokemon.query.filter_by(id=id).first()
    db.session.delete(capturedPokemonToDelete)
    db.session.commit()
    flash("Pokemon deleted succesfully!")
    return redirect(url_for("myTeam"))

@app.route("/myTeam/edit/<id>", methods = ("GET", "POST"))
def myTeamEdit(id):
    intId = int(id)
    capturedPokemonToEdit = CapturedPokemon.query.filter_by(id=id).first()
    print("Captured Pokemon to edit:")
    print(capturedPokemonToEdit)

    if request.method == "POST":
        nickname = request.form["nickname"]
        level = request.form["level"]
        try:
            isShiny = request.form["isShiny"]
            isShiny = True
            print("Is Shiny: " + str(isShiny))
        except BadRequestKeyError:
            isShiny = False
            print("BadRequestKeyError")
            pass

        if not nickname:
            defaultPokemon = getPokemonJsonDataById(capturedPokemonToEdit.idPokemon)
            nickname = defaultPokemon["name"].capitalize()

        isShinyNumber = 0
        if isShiny:
            isShinyNumber = 1

        if not level or int(level) < 1 or int(level) > 100:
            flash("Please provide a level in the range 1-100")
        else:
            capturedPokemonToEdit.nickname = nickname
            capturedPokemonToEdit.level = level
            capturedPokemonToEdit.isShiny = isShinyNumber
            db.session.commit()
            flash("Pokemon edited successfully!")
            return redirect(url_for("myTeam"))

    intId = int(id)
    pokemon = getPokemonJsonDataById(capturedPokemonToEdit.idPokemon)

    return render_template("pokemonAdd.html", pokemon=pokemon, scale=1.4, capturedPokemonToEdit=capturedPokemonToEdit)

def alreadyInPokemons(id):
    for pokemon in pokemons:
        if pokemon["id"] == id:
            return True
    return False

def getPokemonJsonDataById(id):
    if len(pokemons) >= id:
        pokemon = pokemons[id - 1]
    else:
        pokeapi_url = f"https://pokeapi.co/api/v2/pokemon/{id}/"
        r = requests.get(pokeapi_url)
        pokemonJsonData = r.json()
        pokemon = pokemonJsonData
    return pokemon

pokemons = []
pokemonNumberLimit = 251
loading = False

if __name__ == "__main__":
    app.run(debug = True, port = 5000)