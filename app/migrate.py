
from app.config import db_url
from app.models import *
from sqlalchemy_utils import create_database, database_exists

def create_db():
    if not database_exists(db_url):
        create_database(db_url)
        db.drop_all()
        db.create_all()
    add_basic_entries()

def init_db():
    create_db()

def add_basic_entries():
    userRoot = User (
        user = "root",
        password = "root"
    )
    db.session.add(userRoot)

    charizard = CapturedPokemon(
        idPokemon = 6,
        nickname = "Charizard",
        level = 36,
        isShiny = 0
    )
    db.session.add(charizard)

    charizardShiny = CapturedPokemon(
        idPokemon = 6,
        nickname = "Dark",
        level = 58,
        isShiny = 1
    )
    db.session.add(charizardShiny)

    pikachu = CapturedPokemon(
        idPokemon = 25,
        nickname = "Pikachu",
        level = 29,
        isShiny = 0
    )
    db.session.add(pikachu)

    dittoShiny = CapturedPokemon(
        idPokemon = 132,
        nickname = "Delicaditto",
        level = 17,
        isShiny = 1
    )
    db.session.add(dittoShiny)

    db.session.commit()