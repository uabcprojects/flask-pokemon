
from flask import Flask
from app.config import Config
from app.models import db
from app.migrate import init_db
import requests


""" def appendTypes(pokemonTypes):
    text = "Types: "
    for type in pokemonTypes:
        typeName = type['type']['name']
        text += typeName +" "
    return text """

def getColorByType(pokemonType):
    colorByType = {
        "normal": "#a8a878",
        "fire": "#e27e42",
        "water": "#042768",
        "grass": "#78c850",
        "electric": "#f8d030",
        "ice": "#98d8d8",
        "fighting": "#c03028",
        "poison": "#a040a0",
        "ground": "#e0c068",
        "flying": "#a890f0",
        "psychic": "#f85888",
        "bug": "#a8b820",
        "rock": "#b49d3c",
        "ghost": "#705898",
        "dark": "#705848",
        "dragon": "#7039f6",
        "steel": "#b8b8d0",
        "fairy": "#f0b6bc"
    }
    return colorByType[pokemonType]

def castToInt(var):
    return int(var)

def getPokemonById(id, pokemons):
    if len(pokemons) >= id:
        return pokemons[id - 1]
    pokeapi_url = f"https://pokeapi.co/api/v2/pokemon/{id}/"
    r = requests.get(pokeapi_url)
    pokemonJsonData = r.json()
    return pokemonJsonData

def create_app():
    app = Flask(__name__)
    app.config.from_object(Config)
    db.init_app(app)
    #app.jinja_env.globals.update(appendTypes=appendTypes)
    app.jinja_env.globals.update(getColorByType=getColorByType)
    app.jinja_env.globals.update(castToInt=castToInt)
    app.jinja_env.globals.update(getPokemonById=getPokemonById)
    return app