
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class User(db.Model):
    __tablename__ = "user"
    idUser = db.Column(db.Integer, primary_key = True)
    user = db.Column(db.String(45))
    password = db.Column(db.String(45))

""" class Pokemon(db.Model):
    __tablename__ = "pokemon"
    idPokemon = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(45))
    level = db.Column(db.Integer)
    atk = db.Column(db.Integer)
    color = db.Column(db.String(45)) """

class CapturedPokemon(db.Model):
    __tablename__ = "capturedPokemon"
    id = db.Column(db.Integer, primary_key = True)
    idPokemon = db.Column(db.Integer)
    nickname = db.Column(db.String(45))
    level = db.Column(db.Integer)
    isShiny = db.Column(db.Integer)